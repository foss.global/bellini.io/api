import * as plugins from './api.plugins.js';

export interface IBelliniClientOptions {
  publicationName: string;
  smartstatePart?: plugins.smartstate.StatePart<any, IBelliniClientState>;
}

export interface IBelliniClientState {
  articles: plugins.belliniInterfaces.IBelliniArticle[];
}

export class BelliniClient {
  private apiEndpoint = 'https://connect.api.global/bellini';
  public smartstatePart: plugins.smartstate.StatePart<any, IBelliniClientState>;
  public options: IBelliniClientOptions;

  constructor(optionsArg: IBelliniClientOptions) {
    this.options = optionsArg;
    if (optionsArg.smartstatePart) {
      this.smartstatePart = optionsArg.smartstatePart;
    } else {
      const smartstate = new plugins.smartstate.Smartstate();
      this.smartstatePart = smartstate.getStatePart<IBelliniClientState>('bellini', {} as any);
    }
    this.smartstatePart.setState({
      articles: [],
    });
  }

  public async fetchArticles() {
    const getArticles =
      new plugins.typedrequest.TypedRequest<plugins.belliniInterfaces.IRequest_Any_Bellini_GetArticles>(
        this.apiEndpoint,
        'getArticles'
      );
    const response = await getArticles.fire({
      from: Date.now() - 1000000000,
      to: Date.now(),
      publicationName: this.options.publicationName,
    });
    this.smartstatePart.setState({
      articles: response.articles,
    });
    return response.articles;
  }
}
