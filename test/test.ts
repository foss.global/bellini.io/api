import { expect, tap } from '@pushrocks/tapbundle';
import * as api from '../ts/index.js';

let testBelliniClient: api.BelliniClient;

tap.test('should create a valid bellini client', async () => {
  testBelliniClient = new api.BelliniClient({
    publicationName: 'central.eu',
  });

  expect(testBelliniClient).toBeInstanceOf(api.BelliniClient);
});

tap.test('should get articles', async () => {
  await testBelliniClient.fetchArticles();
  console.log(testBelliniClient.smartstatePart.getState());
});

tap.start();
